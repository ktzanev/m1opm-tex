\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{m1opm_beamer}[8/1/2021 class pour le cours de M1 OPM TeX]
% ====================================================================
\LoadClass[aspectratio=43,bigger]{beamer}
% La paramétrisation de Beamer
\setbeamertemplate{navigation symbols}{}% pas de symboles de navigation
\setbeamercovered{transparent} % pour de la semi-transparence à la place de invisible
\setbeamertemplate{headline}{\nointerlineskip%
  \begin{beamercolorbox}[wd=\paperwidth,ht=2ex,dp=1ex]{secsubsec}%
    \tiny\quad\rule[-.1ex]{1.4ex}{1.4ex}\ \strut\insertsection\ $\blacktriangleright$\ \insertsubsection\hfill
    \insertframenumber/\inserttotalframenumber\qquad{}
  \end{beamercolorbox}}
\setbeamertemplate{frametitle}{\nointerlineskip%
  \begin{beamercolorbox}[wd=\paperwidth,ht=1.47ex,dp=0.49ex,left]{frametitle}%
    \normalsize\qquad\strut\insertframetitle%
  \end{beamercolorbox}}
% ====================================================================
% Les package standard (en contion du compilateur)
\usepackage{iftex}
\ifPDFTeX % PDFLaTeX
  \usepackage[utf8]{inputenc} % pour que la source puisse être en utf8
  \usepackage[T1]{fontenc} % pour l'utilisation des polices encodées en T1
  \usepackage{textcomp} % pour utiliser certains caractères
  \usepackage{lmodern} % probablement plus nécéssaire, mais bon...
  \usepackage[scaled=0.9]{DejaVuSansMono} % police mono
\else % LuaLaTeX & XeLaTeX
  \usepackage{fontspec}
    \defaultfontfeatures{Ligatures=TeX} % pour avoir les ligatures standards
    \setmonofont[Scale=0.9]{DejaVu Sans Mono} % police mono
\fi
% --------------------------------------------------------------------
\usepackage{mathtools,amssymb,bbm,amsthm} % les bibliothèques lassiques
\RequirePackage[french]{babel} % pour franciser LaTeX
\usepackage[babel=true]{csquotes} % pour les guillemets
% ====================================================================
% quelques commandes spécifiques à cette présentation
% --------------------------------------------------------------------
% mise en couleurs
\newcommand{\term}[1]{\textcolor{red!70!black}{\emph{#1}}}
\newcommand{\myemph}[1]{\textcolor{green!70!black}{\emph{#1}}}
\setbeamercolor{alerted text}{fg=red!70!black}
% --------------------------------------------------------------------
% pour changer les espacements avant et après les bloques
\addtobeamertemplate{block begin}{%
  \setlength\abovedisplayskip{3pt plus 2pt minus 2pt}%
  \setlength\belowdisplayskip{1pt}%
}{}
% --------------------------------------------------------------------
% pour le code court
\usepackage{fancyvrb}
\fvset{formatcom=\color{red!70!black}}
% \DefineShortVerb{\!}
% \DefineShortVerb{\|}
% pour le code long
\usepackage{listings}
\usepackage{lstautodedent}% pour enlever les espces au début des codes
\lstdefinestyle{latex}{
  language=[LaTeX]TeX,
  morekeywords={part,chapter,subsection,subsubsection,paragraph,subparagraph,chapter*,theoremstyle,includegraphics,graphicspath,fill,draw,path,usetikzlibrary,circle,node,rectangle,ellipse,arc,controls,and,grid,plot,foreach,middle,providecommand,iff,xrightarrow,Long,dfrac,tfrac,cfrac,substack,subarray,sideset,underbracket,overbracket,DeclareMathOperator,operatorname,text,mathbb,bm,mathfrak,mathcal,tag,notag,intertext,eqref,vv,label,captionof,captionsetup,captionbox,coordinate,addplot3,tableofcontents,implies,xshift,yshift,shift,rotate,scale,red,very,thin,ultra,thick,latex,stealth,rounded,corners,domain,samples,smooth,evaluate,surf,opacity
  }
}
\lstdefinestyle{bash}{
  language=bash,
  aboveskip=0pt,
  morekeywords={pdfcpu,gs,gswin32c,magick,convert,mutool,pdfcpu,pdfseparate,pdfunite,pdfimages,pdftocairo,pdftoppm,import,convert,extract,out,cpdf,draw,merge,collect,pdfcrop,img2pdf}
}
\lstset{
  style=latex,
  basicstyle=\color{blue}\ttfamily\tiny,
  keywordstyle=\color{red}\bfseries,
  commentstyle=\color{orange},
  morecomment=[l][\color{white}]{>>>},
  fancyvrb=true,
  autodedent
}
\newcommand*{\seti}[1]{\lstMakeShortInline[columns=fixed,basicstyle=\color{blue}\ttfamily\small]#1}
\newcommand*{\unseti}[1]{\lstDeleteShortInline#1}
\seti{|}
\def\code{\lstinline[columns=fixed,basicstyle=\color{blue}\ttfamily\small]}
% --------------------------------------------------------------------
% pour les page à titre
\newcommand*{\titleframe}[1]{
  \begin{center}\Huge
    #1
  \end{center}
}

\AtBeginPart{\frame{
  \begingroup
    \centering
    {\usebeamerfont{part name}\usebeamercolor[fg]{part name}Partie~\insertpartnumber}
    \vskip1em\par
    \begin{beamercolorbox}[sep=16pt,center]{part title}
      \usebeamerfont{part title}\insertpart\par
    \end{beamercolorbox}
  \endgroup
}}

% \usepackage{etoolbox}

% \robustify{\textit}
% \robustify{\color}


% \def\test{
%   \begin{frame}
%     % \vfill
%     % \centering
%     % \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
%     %     \usebeamerfont{title}Test\par%
%     % \end{beamercolorbox}
%     % \vfill
%   \end{frame}
% }
% --------------------------------------------------------------------
% pour les liens
\usepackage{hyperref}
\hypersetup{colorlinks=true, allcolors=blue}
\newcommand*{\gist}[2]{%
  \href{https://www.overleaf.com/docs?splash=none&snip_uri=https://gist.githubusercontent.com/ktzanev/#1/raw/#2}{\raisebox{-1.4pt}{\includegraphics{overleaf}}}%
  \href{https://gist.githubusercontent.com/ktzanev/#1/raw/#2}{\raisebox{-.7pt}{\includegraphics{download}}}%
}
\newcommand*{\gitlab}[1]{%
  \href{https://www.overleaf.com/docs?splash=none&snip_uri=https://ktzanev.gitlab.io/m1opm-tex/#1}{\raisebox{-1.4pt}{\includegraphics{overleaf}}}%
  \href{https://ktzanev.gitlab.io/m1opm-tex/#1}{\raisebox{-.7pt}{\includegraphics{download}}}%
}


