# Utiliser LaTeX sur votre ordinateur

Pour pouvoir utiliser LaTeX sur votre ordinateur vous avez besoin de deux choses :
- un `éditeur` de code pour éditer vos fichiers `.tex` (ainsi que `.sty`, `.bib`...);
- un `compilateur` qui transforme vos codes `.tex` en `.pdf`


# VS Code (éditeur de code)

Pour l'éditeur de code je vous conseille [VS Code](https://code.visualstudio.com/download), qui pourra vous être utile non seulement pour l'édition des fichiers `.tex`, mais aussi pour l'édition des fichiers `.md`, `.txt`, `.ini`... ainsi que pour la programmation (par exemple en python).

Commencer par installer [VS Code](https://code.visualstudio.com/download) (si vous ne l'avez pas déjà).

Cet éditeur ne vient par défaut qu'avec un support assez limité de LaTeX. Pour cette raison je vous conseille d'installer l'extension `LaTeX Workshop` :
- rendez-vous dans l'onglet des extensions (<kbd>Ctrl+Shift+X</kbd> ou <kbd>Cmd+Shift+X</kbd> sous Mac)
- cherchez `LaTeX Workshop` ;
- cliquer sur `installer`.

Puis rendez-vous dans les paramètres (<kbd>Ctrl+,</kbd>, ou <kbd>Cmd+,</kbd> sous Mac) et changer la valeur de `latex-workshop.latex.recipe.default` à `lastUsed` (au lieu de `first`). Ainsi une fois que vous allez compiler avec `tectonic` les fois d'après il sera le compilateur par défaut.

# Tectonic (compilateur LaTeX)

Pour le compilateur je vous conseille `tectonic` :
- version moderne du compilateur `xelatex`, compatible avec Unicode ;
- il suffit de copier un unique exécutable (pas besoin d'installer une distribution entière) ;
- très rapide ;
- produit des `.pdf` de petite taille.

Au moment d'écrire ces lignes la dernière version de `tectonic` est `0.12.0` et vous pouvez trouver l'exécutable pour votre plate-forme sur [GitHub](https://github.com/tectonic-typesetting/tectonic/releases/tag/tectonic%400.12.0) :
 - [tectonic 0.12.0 pour Linux (.tar.gz)](https://github.com/tectonic-typesetting/tectonic/releases/download/tectonic%400.12.0/tectonic-0.12.0-x86_64-unknown-linux-musl.tar.gz)
 - [tectonic 0.12.0 pour Mac (.tar.gz)](https://github.com/tectonic-typesetting/tectonic/releases/download/tectonic%400.12.0/tectonic-0.12.0-x86_64-apple-darwin.tar.gz)
 - [tectonic 0.12.0 pour Windows (.zip)](https://github.com/tectonic-typesetting/tectonic/releases/download/tectonic%400.12.0/tectonic-0.12.0-x86_64-pc-windows-msvc.zip)

L'archive correspondant ne contient qu'un seul fichier exécutable. Une fois l'archive téléchargé, il faut le décompresser dans un dossier qui se trouve dans le `PATH` (pour qu'il soit accessible de partout, ou voir dans les notes en bas de la page comment faire sinon).

Pour vérifier que `tectonic` est bien installé :
- Dans VS Code ouvrez un terminal ;
- Dans le terminal tapez `tectonic -V`. Si tout va bien, vous devez voir `Tectonic 0.12.0` qui s'affiche.

## Compilation à partir de VSCode

Pour compiler votre fichier `.tex` pour la première fois :
1. Ouvrez un fichier `.tex`, (par exemple [ModeleXeLaTeX.tex](exemples/ModeleXeLaTeX.tex))
1. Rendez-vous dans l'onglet « TeX » (<kbd>Ctrl+Alt+X</kbd> ou <kbd>Cmd+Alt+X</kbd> sous Mac).
1. Dans « Build LaTeX Project » choisissez « Recipe : tectonic ».

À partir de ce moment-là :
- Si vous cliquez sur le petit triangle vert en haut à droite, il va compiler avec `tectonic` (le dernier compilateur utilisé avant).
- À chaque enregistrement de votre fichier, la compilation sera lancée automatiquement. Si vous voulez éviter ce comportement « automatique » (ce qui est recommandé pour les grands projets) vous pouvez modifier le paramètre `latex-workshop.latex.autoBuild.run` en le mettant à `never` (au lieu de `onSave`).

## Notes

- Si vous modifiez `latex-workshop.latex.recipe.default` à `tectonic` (au lieu de `lastUsed`), le petit triangle vert en haut à droite activera la compilation avec `tectonic` toujours (peut importe le compilateur utiliser avant).
- Si `tectonic` n'est pas dans un dossier du `PATH`, vous pouvez :
    1. [Rajouter le dossier où il se trouve dans le `PATH`](images/AddToPath.jpg) ;
    2. ou modifier la commande `"tectonic"` qui se trouve dans le paramètre `latex-workshop.latex.tools` :
        ```json
        "latex-workshop.latex.tools": [
            ...
            {
                "name": "tectonic",
                "command": "tectonic",
                "args": [
                    "--synctex",
                    "--keep-logs",
                    "%DOC%.tex"
                ],
                "env": {}
            }
        ]
        ```
        en indiquant le chemin complet dans `"command": "tectonic"`. Quelque chose comme `"command": "/chemin/vers/tectonic"` ou, sous Windows, `"command": "C:\\chemin\\vers\\tectonic.exe"`.

# Utiliser une distribution complète

Si vous ne voulez/pouvez pas utiliser `tectonic`, vous pouvez installer une des distributions [TeXLive](https://tug.org/texlive/), [MikTeX](https://miktex.org/download) ou [MacTeX](https://www.tug.org/mactex/mactex-download.html). _Attention, la taille de ces distributions est plusieurs centaines de mégaoctets._

## Compilation à partir de VSCode

1. Dans VSCode ouvrer un fichier `.tex` (par exemple [ModeleXeLaTeX.tex](exemples/ModeleXeLaTeX.tex)) pour faire apparaître l'onglet « TeX » sur la barre de gauche.
2. Cliquer sur « TeX » dans la barre de gauche.
3. Sélectionner dans « Commande » le compilateur `xelatex` (ou un autre). À partir de là, normalement, la compilation se fera par défaut avec celui-ci (car c'est le dernier utilisé).

![](images/VSCode_choix_compilateur.png)

# Quelques sources

- [VS Code as LaTeX editor](https://danmackinlay.name/notebook/vs_code_for_latex.html)
- [Configuring Visual Studio Code for LaTeX](https://douglasrizzo.com.br/configuring-vscode-for-latex/)
