# [m1opm-tex](https://gitlab.com/ktzanev/m1opm-tex) / [page web](https://ktzanev.gitlab.io/m1opm-tex/)

Les documents pour l'enseignement de LaTeX du module « Outils pour les professionnels des mathématiques » du L1 Mathématiques à l'Université de Lille.

## Contenu

Dans [ce dépôt](https://gitlab.com/ktzanev/m1opm-tex) vous pouvez trouver les sources LaTeX et les PDFs (compilés avec [tectonic](https://tectonic-typesetting.github.io/en-US/)) des documents suivants :

- **Les slides du cours [[pdf](M1OPM_2023_Cours.pdf)] [[tex](M1OPM_2023_Cours.tex)]**
- [ModeleToutLaTeX.tex](exemples/ModeleToutLaTeX.tex) : modèle de rapport *(pour tout LaTeX)* ;
- [ModeleXeLaTeX.tex](exemples/ModeleXeLaTeX.tex) : modèle de rapport pour `XeLaTeX` et `LuaLaTeX` ;
- [ModelePDFLaTeX.tex](exemples/ModelePDFLaTeX.tex) : modèle de rapport pour `PDFLaTeX` ;
- [ModeleTikZ.tex](exemples/ModeleTikZ.tex) : modèle pour image en Ti*k*Z ;
- [ModelePSTricks.tex](exemples/ModelePSTricks.tex) : modèle pour image en PSTricks *(à compiler avec `XeLaTeX`)*.

Pour compiler la présentation du cours vous avez besoin de la classe [m1opm_beamer.cls](m1opm_beamer.cls), ainsi que des images
 [overleaf.pdf](overleaf.pdf) et [download.pdf](download.pdf).

### Exemples

Ces fichiers servent de base pour illustrer le cours.

- [Exemple_1_structure_generale.tex](exemples/Exemple_1_structure_generale.tex)
- [Exemple_2_theoremes_references.tex](exemples/Exemple_2_theoremes_references.tex)
- [Exemple_3_formules.tex](exemples/Exemple_3_formules.tex)
- [Exemple_4_images.tex](exemples/Exemple_4_images.tex)
- [Exemple_5_complements.tex](exemples/Exemple_5_complements.tex)

## Utilisation

Vous pouvez utiliser [ce dépôt](https://gitlab.com/ktzanev/m1opm-tex) de trois façons faciles :

- en téléchargeant les fichiers un à un ;
- en téléchargeant le [zip](https://gitlab.com/ktzanev/m1opm-tex/-/archive/master/m1opm-tex-master.zip) qui contient la dernière version des fichiers ;
- en clonant le dépôt entier, l'historique y compris, en utilisant la commande `git` suivante
  ```shell
  git clone https://gitlab.com/ktzanev/m1opm-tex.git .
  ```

## LaTeX

Vous pouvez travailler avec LaTeX de deux façons :
- En ligne, et dans ce cas je vous conseille d'utiliser [Overleaf](https://fr.overleaf.com).
- En local, et dans ce cas [je vous conseille d'utiliser VS Code et tectonic](latex_en_local.md). Mais vous aller avoir besoin, dans ce cas, aussi de [Git](https://git-scm.com/download) pour pouvoir partager vos travaux.

## Évaluation

Les critères d'évaluation de vos travaux sont disponibles sur [la page dédiée](criteres_evaluation_LaTeX.md).

## Historique

### 2022

Vous pouvez obtenir [les documents de 2022](https://gitlab.com/ktzanev/m1opm-tex/-/tree/v2022) en téléchargeant le [zip](https://gitlab.com/ktzanev/m1opm-tex/-/archive/v2022/m1opm-tex-v2022.zip) qui contient les dernières versions des fichiers de cette année.

Vous pouvez y trouver les sources LaTeX et les PDFs (compilés avec XeLaTeX ou [tectonic](https://tectonic-typesetting.github.io/en-US/)) des documents suivants :

- **Les slides du cours [[pdf](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2022/M1OPM_2022_Cours.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2022/M1OPM_2022_Cours.tex)]**
- [ModeleToutLaTeX.tex](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2022/exemples/ModeleToutLaTeX.tex) : modèle de rapport *(pour tout LaTeX)* ;
- [ModeleXeLaTeX.tex](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2022/exemples/ModeleXeLaTeX.tex) : modèle de rapport pour `XeLaTeX` et `LuaLaTeX` ;
- [ModelePDFLaTeX.tex](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2022/exemples/ModelePDFLaTeX.tex) : modèle de rapport pour `PDFLaTeX` ;
- [ModeleTikZ.tex](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2022/exemples/ModeleTikZ.tex) : modèle pour image en Ti*k*Z ;
- [ModelePSTricks.tex](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2022/exemples/ModelePSTricks.tex) : modèle pour image en PSTricks *(à compiler avec `XeLaTeX`)*.

Pour compiler la présentation du cours vous avez besoin de la classe [m1opm_beamer.cls](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2022/m1opm_beamer.cls), ainsi que des images
 [overleaf.pdf](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2022/overleaf.pdf) et [download.pdf](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2022/download.pdf).

### 2021

Vous pouvez obtenir [les documents de 2021](https://gitlab.com/ktzanev/m1opm-tex/-/tree/v2021) en téléchargeant le [zip](https://gitlab.com/ktzanev/m1opm-tex/-/archive/v2021/m1opm-tex-v2021.zip) qui contient les dernières versions des fichiers de cette année.

Vous pouvez y trouver les sources LaTeX et les PDFs (compilés avec XeLaTeX ou [tectonic](https://tectonic-typesetting.github.io/en-US/)) des documents suivants :

- Cours 1 [[pdf](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2021/M1OPM_2021_Cours1.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2021/M1OPM_2021_Cours1.tex)], _le 12 janvier 2021_
- Cours 2 [[pdf](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2021/M1OPM_2021_Cours2.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2021/M1OPM_2021_Cours2.tex)], _le 19 janvier 2021_
- Cours 3 [[pdf](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2021/M1OPM_2021_Cours3.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2021/M1OPM_2021_Cours3.tex)], _le 26 janvier 2021_
- Cours 4 [[pdf](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2021/M1OPM_2021_Cours4.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2021/M1OPM_2021_Cours4.tex)], _le 2 février 2021_
- Cours 5 [[pdf](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2021/M1OPM_2021_Cours5.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2021/M1OPM_2021_Cours5.tex)], _le 9 février 2021_
- Cours 6 [[pdf](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2021/M1OPM_2021_Cours6.pdf)] [[tex](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2021/M1OPM_2021_Cours6.tex)], _le 16 février 2021_

Pour compiler ces fichiers vous avez besoin de la classe [m1opm_beamer.cls](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2021/m1opm_beamer.cls), ainsi que des images
 [overleaf.pdf](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2021/overleaf.pdf) et [download.pdf](https://glcdn.githack.com/ktzanev/m1opm-tex/-/raw/v2021/download.pdf).

### Avant

Ce cours est basé sur des cours précédentes :

- [TeX4](https://ktzanev.github.io/tex4lille1/) de 2017
- [TeX412](https://ktzanev.github.io/tex412lille1/) de 2015

## Licence

MIT
